from tornpack.options import define,options,parse_config_file,parse_command_line

define('autoreload',default=True)
define('debug',default=True)
define('env',default='./dev.conf')
define('tornpack_actor_rabbitmq',default={
    'simple_ask':{
        'name':'simple_ask'
    }
})
define('tornpack_actor_rmq_queue',default='tornpack_actor_rmq_%s_%s')
define('tornpack_actor_rmq_rk',default='tornpack.actor.rmq.%s')
define('tornpack_dhtdb_services',default={
    'kodel':'dhtdb_kodel',
    'koget':'dhtdb_koget',
    'koset':'dhtdb_koset'
})
define('tornpack_httpd',default=True)
define('tornpack_httpd_port',default=8088)
define('tornpack_njord',default=False)
define('tornpack_parser_jwt_algorithm_default',default='HS256')
define('tornpack_raid_no_delay',default=True)
define('tornpack_raid_key',default='a9be32142935c1adaab11ed4fb6bff3010c9ce4a')

define('tornpack_dhtdb_codes',default={
    'kget_ok':'612323fdc18e4bbebbadb1a29a9ae827',
    'kget_nok':'f3dfb72dd6b747fbaeddc80970a04a18'
})

define('tornpack_dhtdb_encode',default={
    'ko':'raid::<o:%s><k:%s>'
})

define('tornpack_raid_codes',default={
    'etag':{
        'invalid':'104357bd568743dc9f15bff3722b3eff'
    },
    'service':{
        'not_found':'1526c112f960498c97a24bf2c19b3d8a'
    },
    'welcome':'44bcab4b40274bec8c39377c13a5c549'
})

# mongodb
define('tornpack_mongodb_url',default='mongodb://localhost')

# rabbitmq
define('tornpack_rabbitmq_channel',default={
    'default':'default'
})
define('tornpack_rabbitmq_channel_prefetch_count',default=10)
define('tornpack_rabbitmq_exchange',default={
    'default':'tornpack',
    'headers':'tornpack_headers',
    'headers_any':'tornpack_headers_any'
})
define('tornpack_rabbitmq_name',default='tornpack')
define('tornpack_rabbitmq_url',default='amqp://tornpack:vmakt@loki:5672/tornpack')

# env vm_office
define('vm_office_mongodb_url',default='mongodb://fenrir:27017')
define('vm_office_rabbitmq_url',default='amqp://%s:%s@loki:5672/office')
define('vm_office',default=False)

parse_command_line()
parse_config_file(options.env)

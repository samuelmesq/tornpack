from tornpack.actor.base import Base
from .engine import Engine

__all__ = ['Kernel']

class Kernel(Base):
    __engines = None

    @property
    def engines(self):
        try:
            assert self.__engines
        except AssertionError:
            self.__engines = {}
        except:
            raise
        return self.__engines

    def engine_close(self,name,future=None):
        def on_connection(connection):
            connection.close()
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
            return True
        
        try:
            self.engines[name].connection(self.ioengine.future_instance(on_connection))
        except KeyError:
            pass
        except:
            raise
        else:
            del self.engines[name]
        return True

    def engine(self,name,future):
        try:
            assert self.engines[name]
        except (AssertionError,KeyError):
            self.engines[name] = Engine()
        except:
            raise

        self.engines[name].connection(future)
        return True
    
    def engine_open(self,name,url,future=None):
        try:
            assert name in self.engines
        except AssertionError:
            self.engines[name] = Engine()
        except:
            raise

        self.engines[name].run(name=name,url=url)
        self.engines[name].connection(future)
        return True

Kernel = Kernel()

from tornpack.actor.base import Base

__all__ = ['Publish']

class Publish(Base):
    def push(self,channel,body,exchange,routing_key,properties=None,future=None):
        channel.basic_publish(body=body,exchange=exchange,routing_key=routing_key,properties=properties)
        
        try:
            future.set_result(True)
        except AttributeError:
            pass
        except:
            raise
        return True
    
Publish = Publish()

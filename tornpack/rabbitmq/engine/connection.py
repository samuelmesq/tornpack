from pika import URLParameters
from pika.adapters.tornado_connection import TornadoConnection
from tornpack.actor.base import Base

__all__ = ['Connection']

class Connection(Base):
    @property
    def schema(self):
        return ['url']

    @property
    def validation_ok(self):
        return self.url_parse
   
    def make(self,**kwargs):
        kwargs['swap'] = {'envelop':{}}
        self.ioengine.ioloop.add_callback(self.validate,msg=kwargs)
        return True

    def open(self,**kwargs):
        kwargs = kwargs['msg']

        def on_open(connection):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':kwargs['future'],'swap':{'future':connection}})
            return True

        TornadoConnection(kwargs['swap']['url'],on_open_callback=on_open,stop_ioloop_on_close=False)
        return True

    def url_parse(self,**kwargs):
        kwargs = kwargs['msg']

        try:
            kwargs['swap']['url'] = URLParameters(kwargs['msg']['url'])
            self.ioengine.ioloop.add_callback(self.open,msg=kwargs)
        except:
            raise
        return True

Connection = Connection()

from tornpack.actor.base import Base
from tornpack.options import options
from .connection import Connection
from .channel import Channel
from .consumer import Consumer
from .exchange import Exchange
from .publish import Publish
from .queue import Queue

__all__ = ['Engine']

class Engine(Base):
    __channels = None
    __channels_future = None
    __connection = None
    name = None
    url = None

    @property
    def channels(self):
        try:
            assert self.__channels
        except AssertionError:
            self.__channels = {}
        except:
            raise
        return self.__channels

    @property
    def connection_io(self):
        return self.is_ready

    @connection_io.setter
    def connection_io(self,_):
        self.is_ready = _

    @property
    def consumer(self):
        return Consumer

    @property
    def exchange(self):
        return Exchange

    @property
    def queue(self):
        return Queue

    @property
    def publish(self):
        return Publish

    def __connect(self):
        def on_connect(result):
            try:
                assert result.result()
            except:
                self.ioengine.ioloop.add_callback(self.connect)
                raise
            else:
                self.__connection = result.result()
                self.is_ready = self
            return True

        Connection.run(msg={'url':self.url},future=self.ioengine.future_instance(on_connect))
        return True

    def channel(self,name,future,prefetch_count=10):
        try:
            assert self.channels[name].is_ready
        except AssertionError:
            self.channels[name].promises.append(future)
        except KeyError:
            def on_connection(result):
                try:
                    assert name in self.channels
                except AssertionError:
                    self.channels[name] = Channel()
                    self.channels[name].run(msg={'name':name,'connection':self.__connection,'prefetch_count':prefetch_count})
                    self.channels[name].promises.append(future)
                except:
                    raise
                else:
                    self.ioengine.ioloop.add_callback(self.channel,name,future,prefetch_count)
                return True

            self.connection(self.ioengine.future_instance(on_connection))
        except:
            raise
        else:
            future.set_result(self.channels[name].io)
        return True

    def connection(self,future):
        try:
            assert self.is_ready
        except AssertionError:
            self.promises.append(future)
        except:
            raise
        else:
            future.set_result(self)
        return True

    def make(self,**kwargs):
        try:
            self.name = kwargs['name']
            self.url = kwargs['url']
        except KeyError:
            raise
        except:
            raise

        self.ioengine.ioloop.add_callback(self.__connect)
        return True

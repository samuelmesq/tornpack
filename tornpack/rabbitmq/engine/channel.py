from tornpack.actor.base import Base

__all__ = ['Channel']

class Channel(Base):
    name = None

    @property
    def schema(self):
        return ['connection']

    @property
    def validation_ok(self):
        return self.name_check
    
    def make(self,**kwargs):
        kwargs['swap'] = {'envelop':{}}
        self.ioengine.ioloop.add_callback(self.validate,msg=kwargs)
        return True    

    def name_check(self,**kwargs):
        kwargs = kwargs['msg']
        
        try:
            self.name = kwargs['msg']['name']
        except KeyError:
            self.name = self.ioengine.uuid4
        except:
            raise
        
        self.ioengine.ioloop.add_callback(self.open,msg=kwargs)
        return True

    def open(self,**kwargs):
        kwargs = kwargs['msg']

        def on_connect(channel):
            kwargs['channel'] = channel
            kwargs['swap']['future'] = kwargs['channel']
            self.ioengine.ioloop.add_callback(self.qos_set,msg=kwargs)
            return True
        
        kwargs['msg']['connection'].channel(on_connect)
        return True
    
    def qos_set(self,**kwargs):
        kwargs = kwargs['msg']

        def on_qos(result):
            self.is_ready = kwargs['channel']
            self.ioengine.ioloop.add_callback(self.future,msg=kwargs)
            return True

        kwargs['channel'].basic_qos(callback=on_qos,prefetch_count=kwargs['msg']['prefetch_count'])
        return True

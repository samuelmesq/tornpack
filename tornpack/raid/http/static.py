from hashlib import sha1,sha256

__all__ = [
    'secret'
]

def secret(_,salt,fix=options.tornpack_name):
    return sha256('<%s>::%s<%s>' % (sha1(_).hexdigest(),sha1(salt).hexdisgest(),sha1(fix).hexdigest())).hexdisgest()

from tornado.websocket import WebSocketClosedError,WebSocketHandler
from tornpack.actor.base import Base
from tornpack.options import options
from tornpack.parser.jwt import decode,DecodeError,encode
from tornpack.raid import Kernel
from tornpack.raid.actions import actions as ACTIONS
from tornpack.raven import Raven

__all__ = ['Handler']

class Handler(WebSocketHandler,Base):
    __actions = None
    __name = None
    __secret = None
    __uid = None
    __welcome = None

    @property
    def actions(self):
        try:
            assert self.__actions
        except AssertionError:
            self.__actions = {}
        except:
            raise
        return self.__actions

    @actions.setter
    def actions(self,_):
        try:
            assert isinstance(_,dict)
        except AssertionError:
            pass
        except:
            raise
        else:
            self.__actions = _

    @property
    def kernel(self):
        return Kernel

    @property
    def name(self):
        try:
            assert self.__name
        except AssertionError:
            return 'raid'
        except:
            raise
        return self.__name

    @name.setter
    def name(self,_):
        self.__name = _

    @property
    def secret(self):
        try:
            assert self.__secret
        except AssertionError:
            self.__secret = options.tornpack_raid_key
        except:
            raise
        return self.__secret

    @secret.setter
    def secret(self,_):
        self.__secret = _

    @property
    def uid(self):
        try:
            assert self.__uid
        except AssertionError:
            self.__uid = self.ioengine.uuid4
        except:
            raise
        return self.__uid

    def __message_decode__(self,msg,future):
        try:
            payload = decode(msg,self.secret)
        except DecodeError:
            self.ioengine.ioloop.add_callback(self.disconnect)
            payload = False
        except:
            raise

        self.ioengine.ioloop.add_callback(self.future,
                                          msg={'future':future,
                                               'swap':{'future':payload}})
        return True

    def __on_close__(self):
        pass

    def __on_disconnect__(self):
        pass

    def __on_message__(self,msg):
        try:
            assert msg['header']['action'] in ACTIONS
        except (AssertionError,KeyError):
            self.ioengine.ioloop.add_callback(self.on_msg,msg=msg)
        except:
            raise
        else:
            ACTIONS[msg['header']['action']](msg,io=self)
        return True

    def __on_open__(self):
        pass

    def __push__(self,message,secret):
        self.write_message(encode(message,secret))
        return True

    def __welcome__(self,msg):
        def auth():
            try:
                assert msg['header']['action'] == 'welcome'
            except AssertionError:
                self.ioengine.ioloop.add_callback(self.disconnect)
            except:
                raise
            else:
                ACTIONS['welcome'](msg,io=self)
            return True

        try:
            assert self.__welcome
        except AssertionError:
            self.ioengine.ioloop.add_callback(auth)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.__on_message__,msg=msg)
        return True

    def check_origin(self,origin):
        return True

    def disconnect(self):
        self.close()
        self.ioengine.ioloop.add_callback(self.__on_disconnect__)
        return True

    def error(self,etag,code):
        self.ioloop.add_callback(self.push,message={
            'header':{
                'action':'error',
                'etag':etag,
                'code':code
            }
        })
        return True

    def on_message(self,msg):
        def on_decode(result):
            try:
                assert result.result()
            except AssertionError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(self.__welcome__,msg=result.result())
            return True

        self.ioengine.ioloop.add_callback(self.__message_decode__,
                                          msg=msg,
                                          future=self.ioengine.future_instance(on_decode))
        return True

    def open(self):
        Raven.info('raid - %s connected [%s]' % (self.request.remote_ip,self.uid))
        self.set_nodelay(options.tornpack_raid_no_delay)
        ACTIONS['welcome'](msg={'header':{'method':'who'}},io=self)
        self.kernel.connection_add(self)
        self.ioengine.ioloop.add_callback(self.__on_open__)
        return None

    def on_close(self):
        Raven.info('raid - %s closed [%s]' % (self.request.remote_ip,self.uid))
        self.kernel.connection_del(self)
        self.ioengine.ioloop.add_callback(self.__on_close__)
        return True

    def push(self,message):
        self.__push__(message,self.secret)
        return True

    def to_push(self):
        def future(result):
            self.ioengine.ioloop.add_callback(self.push,result.result())
            return True

        return self.ioengine.future_instance(future)

    def utag(self,etag):
        return sha1('%s:%s:%s' % (self.secret,self.name,etag)).hexdigest()

    def welcome_done(self,_):
        try:
            assert _
        except:
            self.__welcome = False
        else:
            self.__welcome = True
        return True

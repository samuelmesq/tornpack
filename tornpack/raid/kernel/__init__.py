from tornpack.actor.base import Base

__all__ = ['Kernel']

class Kernel(Base):
    __connections = None
    __groups = None

    @property
    def connections(self):
        try:
            assert self.__connections
        except AssertionError:
            self.__connections = {}
        except:
            raise
        return self.__connections

    @property
    def groups(self):
        try:
            assert self.__groups
        except AssertionError:
            self.__groups = {}
        except:
            raise
        return self.__groups

    def __group__(self,group,foreach,future):
        try:
            io = group.next()
        except StopIteration:
            future.set_result(True)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(foreach,io)
            self.ioengine.ioloop.add_callback(self.__group__,
                                              group=group,
                                              foreach=foreach,
                                              future=future)
        return True
    
    def connection_add(self,io):
        self.connections[io.uid] = io
        return True

    def connection_del(self,io):
        try:
            assert io.uid in self.connections
        except AssertionError:
            pass
        except:
            raise
        else:
            del self.connections[io.uid]
        return True

    def group_add(self,group,io):
        try:
            assert group in self.groups
        except AssertionError:
            self.groups[group] = {io.uid:io}
        except:
            raise
        else:
            self.groups[group][io.uid] = io
        return True

    def group(self,group,foreach,future):
        try:
            assert group in self.groups
        except AssertionError:
            future.set_result(False)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.__group__,
                                              group=iter(group),
                                              foreach=foreach,
                                              future=future)
        return True
    
    def group_broadcast(self,group,msg,future):
        def push(io):
            io.push(msg)
            return True
        
        self.ioengine.ioloop.add_callback(self.group,
                                          foreach=push,
                                          future=future)
        return True
    
    def group_del(self,group,io):
        try:
            assert uid in self.groups[group]
        except AssertionError:
            pass
        except KeyError:
            pass
        except:
            raise
        else:
            del self.groups[group][uid]            
        return True

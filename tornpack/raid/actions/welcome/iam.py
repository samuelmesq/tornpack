from datetime import datetime
import static
from tornpack.actor.base import Base
from tornpack.options import options
from tornpack.parser.date import datetime2timestamp

__all__ = ['IAM']

class IAM(Base):
    def init(self,msg,io):
        try:
            assert msg['body']['datetime']
            assert msg['body']['name']
        except (AssertionError,KeyError):
            self.ioengine.ioloop.add_callback(io.disconnect)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(
                self.key_gen,
                name=msg['body']['name'],
                dtime=msg['body']['datetime'],
                io=io
            )
        return True

    def key_gen(self,name,dtime,io):
        key = static.key_gen(name,dtime)
        io.push({
            "header":{
                "action":"welcome",
                "method":"take",
                "__sysdate__":datetime2timestamp(datetime.utcnow())
            },
            "body":{
                "key":key
            }
        })
        io.secret = key
        io.welcome_done(True)
        return True

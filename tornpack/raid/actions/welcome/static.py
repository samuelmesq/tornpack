from hashlib import sha256
from uuid import uuid4

__all__ = ['key_gen']

def key_gen(root,salt):
    return sha256('%s<%s>:%s' % (root,salt,uuid4)).hexdigest()

from .koget import KOGet
from .koset import KOSet
from tornpack.actor.base import Base

__all__ = ['DHTDB']

class DHTDB(Base):
    __actions = {
        'koget':KOGet().init,
        'koset':KOSet().init
    }

    @property
    def actions(self):
        return self.__actions

    def on_msg(self,msg,io):
        try:
            assert msg['header']['method'] in self.actions
        except (AssertionError,KeyError):
            self.ioengine.ioloop.add_callback(io.disconnect)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.actions[msg['header']['method']],msg=msg,io=io)
        return True

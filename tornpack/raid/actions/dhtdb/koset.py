from datetime import datetime
from .static import ko as KO_ENCODE
from tornpack.actor.base import Base
from tornpack.actor.rabbitmq import SimpleAsk
from tornpack.actor.rabbitmq.static import routing_key
from tornpack.options import options
from tornpack.parser.date import datetime2timestamp
from tornpack.parser.json import jsonify

__all__ = ['KOSet']

class KOSet(Base):
    def __hash(self,o,k):
        return KO_ENCODE(o,k)
    
    def ask(self,msg,io):
        def on_response(result):
            self.ioengine.ioloop.add_callback(self.response,io=io,response=result.result())
            return True
        
        SimpleAsk(
            etag=msg['header']['etag'],
            future=self.ioengine.future_instance(on_response),
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key=routing_key(options.tornpack_dhtdb_services['koset']),
            body=jsonify(msg['body'])
        )
        return True
    
    def init(self,msg,io):
        try:
            assert msg['body']['k']
            assert msg['body']['o']
            assert msg['body']['v']
        except (AssertionError,KeyError):
            self.ioengine.ioloop.add_callback(io.disconnect)
        except:
            raise
        else:
            msg['body']['o'] = self.__hash(msg['body']['o'],msg['body']['k'])
            self.ioengine.ioloop.add_callback(self.ask,msg=msg,io=io)
        return True

    def response(self,io,response):
        io.push({
            "header":{
                "__sysdate__":datetime2timestamp(datetime.utcnow()),
                "action":"dhtdb",
                "code":response['properties'].headers['code'],
                "etag":response['properties'].headers['etag'],
                "method":"koset"
            }
        })
        return True

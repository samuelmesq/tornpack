from .dhtdb import DHTDB
from .uuid import UUID
from welcome import Welcome

__all__ = ['actions']

actions = {
    'dhtdb':DHTDB().on_msg,
    'uuid':UUID().init,
    'welcome':Welcome().on_msg
}

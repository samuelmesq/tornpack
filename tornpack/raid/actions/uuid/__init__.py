from datetime import datetime
from tornpack.actor.base import Base
from tornpack.parser.date import datetime2timestamp

__all__ = ['UUID']

class UUID(Base):
    def init(self,msg,io):
        try:
            assert msg['header']['etag']
        except (AssertionError,KeyError):
            return False
        except:
            raise
        else:
            io.push({
                "header":{
                    "action":"uuid",
                    "etag":msg['header']['etag'],
                    "__sysdate__":datetime2timestamp(datetime.utcnow())
                },
                "body":self.ioengine.uuid4
            })
        return True

from tornado.web import Application
from tornado.options import options
from tornpack.actor.base import Base
from tornpack.raven import Raven

__all__ = ['HTTPD']

class HTTPD(Base):
    __started = None
    __server = None
    
    def add_handler(self,pattern=r"",handlers=[],future=None):
        def on_server(result):
            result.result().add_handlers(pattern,handlers)
            
            try:
                assert future
            except:
                pass
            else:
                future.set_result(True)
            return True        
        
        self.server(self.ioengine.future_instance(on_server))
        return True

    def server(self,future):
        try:
            assert self.__server
        except AssertionError:
            self.__server = Application()
        except:
            raise

        future.set_result(self.__server)
        return True
    
    def start(self,future,port=options.tornpack_httpd_port):
        def on_server(result):
            result.result().listen(port)
            Raven.info('httpd - start: %s' % port)
            self.__started = True
            future.set_result(True)
            return True
       
        try:
            assert self.__started
        except AssertionError:
            self.server(self.ioengine.future_instance(on_server))
        except:
            raise
        else:
            future.set_result(True)
        return True

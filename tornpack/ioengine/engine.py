from tornado.concurrent import Future
from tornado.ioloop import IOLoop, PeriodicCallback
from uuid import uuid4

__all__ = ['Engine']

class Engine(object):
    __routines = None

    @property
    def ioloop(self):
        return IOLoop.instance()

    @property
    def uuid4(self):
        return uuid4().hex

    @property
    def routines(self):
        try:
            assert self.__routines
        except AssertionError:
            self.__routines = {}
        except:
            raise
        return self.__routines

    def add_routine(self, time, event):
        uuid = self.uuid4
        self.routines[uuid] = PeriodicCallback(event, time)
        self.routines[uuid].start()
        return uuid

    def future_instance(self,actor):
        future = Future()
        self.ioloop.add_future(future, actor)
        return future

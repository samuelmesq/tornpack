from tornpack.ioengine import IOEngine
from tornpack.parser.protobuf import decode as protobuf_decode, encode as protobuf_encode

__all__ = ['Base']

class Base(object):
    __promises = None
    __uid = None
    io = None

    @property
    def ioengine(self):
        return IOEngine

    @property
    def is_ready(self):
        return self.io

    @is_ready.setter
    def is_ready(self,_):
        try:
            assert _
        except AssertionError:
            self.io = None
        except:
            raise
        else:
            self.io = _
            self.ioengine.ioloop.add_callback(self.__promises_consumer__)

    @property
    def promises(self):
        try:
            assert self.__promises
        except AssertionError:
            self.__promises = []
        except:
            raise
        return self.__promises

    @property
    def schema(self):
        return []

    @property
    def uid(self):
        try:
            assert self.__uid
        except AssertionError:
            self.__uid = self.ioengine.uuid4
        except:
            raise
        return self.__uid

    @property
    def validate_ok(self):
        return None

    def __future_after__(self,**kwargs):
        pass

    def __promises_consumer__(self):
        try:
            assert self.promises
            self.promises.pop(0).set_result(self.io)
        except (AssertionError,AttributeError):
            pass
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.__promises_consumer__)
        return True

    def __protobuf_decode__(self,payload,proto,future):
        try:
            result = protobuf_decode(payload,proto)
        except:
            result = False
            raise
        finally:
            future.set_result(result)
        return True

    def __protobuf_encode__(self,proto,future):
        try:
            result = protobuf_encode(proto)
        except:
            result = False
            raise
        finally:
            future.set_result(result)
        return True

    def future(self,**kwargs):
        kwargs = kwargs['msg']

        try:
            assert 'future' in kwargs
            kwargs['future'].set_result(kwargs['swap']['future'])
        except (AssertionError,AttributeError,TypeError):
            pass
        except KeyError:
            kwargs['future'].set_result(None)
        except:
            raise

        self.ioengine.ioloop.add_callback(self.__future_after__,msg=kwargs)
        return True

    def make(self,**kwargs):
        pass

    def run(self,**kwargs):
        self.ioengine.ioloop.add_callback(self.make,**kwargs)
        return True

    def validate(self,**kwargs):
        kwargs = kwargs['msg']

        try:
            key = kwargs['swap']['schema_iter'].next()
            assert key in kwargs['msg']
            kwargs['swap']['envelop'][key] = kwargs['msg'][key]
        except (AssertionError,TypeError):
            self.ioengine.ioloop.add_callback(self.future,msg=kwargs)
            return False
        except KeyError:
            kwargs['swap']['schema_iter'] = iter(self.schema)
        except StopIteration:
            self.ioengine.ioloop.add_callback(self.validation_ok,msg=kwargs)
            return True
        except:
            raise

        self.ioengine.ioloop.add_callback(self.validate,msg=kwargs)
        return None

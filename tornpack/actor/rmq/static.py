from hashlib import sha1
from tornpack.options import options

__all__ = [
    'channel_name',
    'name_hash',
    'queue',
    'routing_key'
]

def channel_name(name,uid):
    return sha1('%s::%s' % (name,uid)).hexdigest()

def name_hash(_):
    return sha1(_).hexdigest()

def queue(_):
    return options.tornpack_actor_rmq_queue % (_,name_hash(_))

def routing_key(_):
    return options.tornpack_actor_rmq_rk % name_hash(_)

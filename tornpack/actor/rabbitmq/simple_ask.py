from .channel import Channel
from .connection import Connection
from .exchange import Exchange
from pika import BasicProperties
from .queue import Queue
from tornpack.options import options

__all__ = ['SimpleAsk']

class SimpleAsk(Channel,Connection,Exchange,Queue):
    __etag = None
    __name = None
    __rabbitmq_channel_prefetch_count = None
    __rabbitmq_queue_arguments = None

    @property
    def etag(self):
        return self.__etag
    
    @property
    def name(self):
        return self.__name

    @property
    def queue(self):
        return self.__rabbitmq_queue_name__(self.name)

    @property
    def rabbitmq_queue_arguments(self):
        try:
            assert self.__rabbitmq_queue_arguments
        except AssertionError:
            self.__rabbitmq_queue_arguments = {'exclusive':False,'durable':True}
        except:
            raise
        return self.__rabbitmq_queue_arguments

    @property
    def tell(self):
        return self.__tell.set_result
    
    def __init__(self,etag,future,body='',exchange=options.tornpack_rabbitmq_exchange['default'],routing_key='',headers={}):
        self.__etag = etag
        self.__name = options.tornpack_actor_rabbitmq['simple_ask']['name']
        self.__payload = {'body':body,'exchange':exchange,'routing_key':routing_key,'properties':BasicProperties(headers=headers)}
        self.__tell = future
        self.ioengine.ioloop.add_callback(self.__on_init__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_connection__)

    def __ask__(self):
        self.__payload['properties'].headers['etag'] = self.etag
        self.__payload['properties'].headers['__simple_ask__'] = True
        
        self.rabbitmq_channel.basic_publish(
            **self.__payload
        )
        return True
        
    def __on_init__(self):
        pass

    def __on_rabbitmq_channel__(self,channel):
        self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_declare_default__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_declare_headers__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_declare_headers_any__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_declare_default__)
        return True

    def __on_rabbitmq_connection__(self,connection):
        self.ioengine.ioloop.add_callback(self.__rabbitmq_channel__)
        return True

    def __rabbitmq_consumer_set_on_default__(self):
        def on_msg(channel,deliver,properties,body):
            def ack(result):
                try:
                    assert result.result()
                except AssertionError:
                    channel.basic_nack(deliver.delivery_tag)
                except:
                    raise
                else:
                    channel.basic_ack(deliver.delivery_tag)
                return True

            self.ioengine.ioloop.add_callback(
                self.on_message,
                msg={
                    'channel':channel,
                    'deliver':deliver,
                    'properties':properties,
                    'body':body,
                    'ack':self.ioengine.future_instance(ack)
                }
            )
            return True

        self.rabbitmq_channel.basic_consume(
            consumer_callback=on_msg,
            queue=self.queue
        )
        return True

    def __rabbitmq_exchange_declare_default__(self):
        def on_declare(result):
            return True

        self.__rabbitmq_exchange_declare__(
            exchange=options.tornpack_rabbitmq_exchange['default'],
            exchange_type='topic',
            future=self.ioengine.future_instance(on_declare),
            durable=True
        )
        return True

    def __rabbitmq_exchange_declare_headers__(self):
        def on_declare(result):
            return True

        self.__rabbitmq_exchange_declare__(
            exchange=options.tornpack_rabbitmq_exchange['headers'],
            exchange_type='headers',
            future=self.ioengine.future_instance(on_declare),
            durable=True,
            arguments={'x-match':'all'}
        )
        return True

    def __rabbitmq_exchange_declare_headers_any__(self):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind_etag__)
            return True

        self.__rabbitmq_exchange_declare__(
            exchange=options.tornpack_rabbitmq_exchange['headers_any'],
            exchange_type='headers',
            future=self.ioengine.future_instance(on_declare),
            durable=True
        )
        return True

    def __rabbitmq_queue_bind_etag__(self):
        def on_bind(result):
            self.ioengine.ioloop.add_callback(self.__ask__)
            return True
        
        self.__rabbitmq_queue_bind__(
            queue=self.queue,
            exchange=options.tornpack_rabbitmq_exchange['headers_any'],
            routing_key=self.__rabbitmq_routing_key__(self.name),
            arguments={'etag':self.etag},
            future=self.ioengine.future_instance(on_bind)
        )
        return True
       
    def __rabbitmq_queue_bind_exchange__(self):
        self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind_exchange_topic__)
        return True

    def __rabbitmq_queue_bind_exchange_topic__(self):
        def on_bind(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_consumer_set_on_default__)
            return True

        self.__rabbitmq_queue_bind__(
            queue=self.queue,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key=self.__rabbitmq_routing_key__(self.name),
            future=self.ioengine.future_instance(on_bind)
        )
        return True

    def __rabbitmq_queue_declare_default__(self):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind_exchange__)
            return True

        self.__rabbitmq_queue_declare__(
            queue=self.queue,
            future=self.ioengine.future_instance(on_declare),
            exclusive=True,
            auto_delete=True
        )
        return True

    def on_message(self,msg):
        try:
            assert msg['properties'].headers['__simple_ask__']
        except (AssertionError,KeyError):
            msg['ack'].set_result(True)
            self.tell(msg)
            self.ioengine.ioloop.add_callback(self.__rabbitmq_channel_close__)
        except:
            raise
        else:
            msg['ack'].set_result(True)
        return True

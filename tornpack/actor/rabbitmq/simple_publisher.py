from pika.spec import BasicProperties
from tornpack.options import options
from .channel import Channel
from .connection import Connection

__all__ = ['SimplePublisher']

class SimplePublisher(Channel,Connection):
    def __init__(self):
        self.name = self.uid
        self.ioengine.ioloop.add_callback(self.__on_init__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_connection__)

    def __on_init__(self):
        pass

    def __on_rabbitmq_connection__(self,connection):
        self.ioengine.ioloop.add_callback(self.__rabbitmq_channel__)
        return True

    def __properties(self,props):
        try:
            assert isinstance(props,BasicProperties)
        except AssertionError:
            return BasicProperties(**props)
        except:
            raise
        return props

    def __publish(self,body,future,exchange,routing_key,properties):
        def on_channel(channel):
            try:
                channel.result().basic_publish(body=body,
                                               exchange=exchange,
                                               routing_key=routing_key,
                                               properties=self.__properties(properties))
            except:
                self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':False}})
                raise
            else:
                self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
            return True

        self.__rabbitmq_channel__(future=self.ioengine.future_instance(on_channel))
        return True
    
    def __publish__(self,body,future,exchange=options.tornpack_rabbitmq_exchange['default'],routing_key='',properties={'headers':{}}):

        try:
            assert self.rabbitmq_connection
        except AssertionError:
            def on_conn(conn):
                self.ioengine.ioloop.add_callback(self.__publish__,
                                                  body=body,
                                                  future=future,
                                                  exchange=exchange,
                                                  routing_key=routing_key,
                                                  properties=properties)
                return True

            self.__rabbitmq_connection__(self.ioengine.future_instance(on_conn))
        except:
            raise
        else:
            self.__publish(body=body,
                           future=future,
                           exchange=exchange,
                           routing_key=routing_key,
                           properties=properties)
        return True

from tornpack.actor.base import Base

__all__ = ['Exchange']

class Exchange(Base):
    def __on_rabbitmq_exchange_declare__(self,exchange):
        pass
    
    def __rabbitmq_exchange_declare(self,channel,exchange,exchange_type,future,passive=False,durable=False,auto_delete=False,internal=False,arguments=None):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':True}})
            self.ioengine.ioloop.add_callback(self.__on_rabbitmq_exchange_declare__,exchange=exchange)
            return True
        
        channel.exchange_declare(
            exchange=exchange,
            exchange_type=exchange_type,
            passive=passive,
            durable=durable,
            auto_delete=auto_delete,
            internal=internal,
            arguments=arguments,
            callback=on_declare
        )
        return True

    def __rabbitmq_exchange_declare__(self,exchange,exchange_type,future,passive=False,durable=False,auto_delete=False,internal=False,arguments=None):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':True}})
            return True

        try:
            assert self.rabbitmq_channel
        except:
            raise
        else:
            self.__rabbitmq_exchange_declare(
                channel=self.rabbitmq_channel,
                exchange=exchange,
                exchange_type=exchange_type,
                future=self.ioengine.future_instance(on_declare),
                passive=passive,
                durable=durable,
                auto_delete=auto_delete,
                internal=internal,
                arguments=arguments
            )
        return True

from tornpack.actor.base import Base
from tornpack.options import options
from tornpack.rabbitmq import RabbitMQ

__all__ = ['Connection']

class Connection(Base):
    __RABBITMQ_CONNECTION = None

    @property
    def rabbitmq_connection(self):
        return self.__RABBITMQ_CONNECTION

    def __on_rabbitmq_connection__(self,connection):
        pass
    
    def __rabbitmq_connection(self,future):
        def on_connection(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result.result()}})
            self.ioengine.ioloop.add_callback(self.__on_rabbitmq_connection__,connection=result.result())
            return True
        
        RabbitMQ.engine(options.tornpack_rabbitmq_name,self.ioengine.future_instance(on_connection))
        return True

    def __rabbitmq_connection__(self,future=None):
        def on_connection(result):
            self.__RABBITMQ_CONNECTION = result.result()
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result.result()}})
            return True
        
        self.ioengine.ioloop.add_callback(self.__rabbitmq_connection,
                                          future=self.ioengine.future_instance(on_connection))
        return True
    

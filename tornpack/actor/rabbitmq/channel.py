import static
from tornpack.actor.base import Base
from tornpack.options import options

__all__ = ['Channel']

class Channel(Base):
    __name = None
    __RABBITMQ_CHANNEL = None
    __RABBITMQ_CHANNEL_PREFETCH_COUNT = None

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self,_):
        self.__name = _
    
    @property
    def rabbitmq_channel(self):
        return self.__RABBITMQ_CHANNEL

    @property
    def rabbitmq_channel_prefetch_count(self):
        try:
            assert self.__RABBITMQ_CHANNEL_PREFETCH_COUNT
        except AssertionError:
            self.__RABBITMQ_CHANNEL_PREFETCH_COUNT = options.tornpack_rabbitmq_channel_prefetch_count
        except:
            raise
        return self.__RABBITMQ_CHANNEL_PREFETCH_COUNT

    @property
    def rabbitmq_channel_name(self):
        try:
            assert self.name
        except AssertionError:
            raise
        except:
            raise
        return static.channel_name(self.name,self.uid)
    
    def __on_rabbitmq_channel__(self,channel):
        pass
    
    def __rabbitmq_channel(self,connection,name,future,prefetch_count):
        def on_channel(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':result.result()}})
            self.ioengine.ioloop.add_callback(self.__on_rabbitmq_channel__,channel=result.result())
            return True

        connection.channel(
            future=self.ioengine.future_instance(on_channel),
            prefetch_count=prefetch_count,
            name=name
        )
        return True

    def __rabbitmq_channel__(self,future=None):
        def on_channel(result):
            self.__RABBITMQ_CHANNEL = result.result()
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result.result()}})
            return True
    
        try:
            assert self.rabbitmq_connection
        except:
            raise
        else:
            self.__rabbitmq_channel(
                connection=self.rabbitmq_connection,
                name=self.rabbitmq_channel_name,
                future=self.ioengine.future_instance(on_channel),
                prefetch_count=self.rabbitmq_channel_prefetch_count
            )
        return True

    def __rabbitmq_channel_close__(self):
        self.rabbitmq_channel.close()
        return True

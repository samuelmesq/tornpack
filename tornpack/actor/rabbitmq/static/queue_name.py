from hashlib import sha1
from tornpack.options import options

__all__ = ['queue_name']

def queue_name(_):
    return options.tornpack_actor_rmq_queue % (_,sha1(_).hexdigest())

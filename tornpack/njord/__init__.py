from kernel import Kernel as Njord
from tornpack.options import options

__all__ = ['Njord']

try:
    assert options.tornpack_njord
except AssertionError:
    pass
except:
    raise
else:
    Njord = Njord(name=options.tornpack_name,
                  queue_arguments={'exclusive':True,'auto_delete':True,'durable':False})
    Njord.start()

from hashlib import sha1
from .pb import Request as RequestPB, Response as ResponsePB
from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.options import options
from tornpack.parser.json import jsonify
from tornpack.parser.protobuf import pb2d

__all__ = ['Request']

class Request(Base):
    @property
    def name(self):
        return 'njord_http'
        
    @property
    def schema(self):
        return ['url']

    @property
    def validate_ok(self):
        return self.method_parser
    
    @property
    def uid(self):
        return sha1('%s:%s<%s>' % (self.name,self.ioengine.uuid4,self.ioengine.uuid4)).hexdigest()

    def ask(self,**kwargs):
        def on_ask(result):
            try:
                assert result.result()
            except AssertionError:
                kwargs['future'].set_result(False)
            except:
                raise
            else:
                def on_decode(result):
                    kwargs['future'].set_result(pb2d(result.result()))
                    return True
                
                self.__protobuf_decode__(result.result(),ResponsePB(),self.ioengine.future_instance(on_decode))
            return True
        
        Njord.publish(name=self.name,
                      body=kwargs['swap']['request'],
                      future=self.ioengine.future_instance(on_ask),
                      uid=kwargs['swap']['uid'])
        return True

    def headers_parser(self,**kwargs):
        try:
            assert kwargs['msg']['headers']
        except AssertionError:
            del kwargs['msg']['headers']
        except KeyError:
            pass
        except:
            raise
        else:
            kwargs['msg']['headers'] = jsonify(kwargs['msg']['headers'])

        self.ioengine.ioloop.add_callback(self.payload,**kwargs)
        return True
    
    def make(self,url,future,**kwargs):
        msg = {'future':future,
               'msg':kwargs,
               'swap':{'uid':self.uid}}

        msg['msg']['url'] = url
        self.ioengine.ioloop.add_callback(self.headers_parser,**msg)
        return True

    def payload(self,**kwargs):
        kwargs['swap']['request'] = RequestPB(uid=kwargs['swap']['uid'],**kwargs['msg'])
        self.ioengine.ioloop.add_callback(self.ask,**kwargs)
        return True
    

from pika.spec import BasicProperties
from tornpack.actor.rmq import RMQ
from tornpack.actor.rmq.static import routing_key
from tornpack.options import options

__all__ = ['Kernel']

class Kernel(RMQ):
    __futures = None

    @property
    def futures(self):
        try:
            assert self.__futures
        except AssertionError:
            self.__futures = {}
        except:
            raise
        return self.__futures

    def __bind(self,name,body,future,uid,headers):
        def on_bind(result):
            self.ioengine.ioloop.add_callback(self.__publish,
                                              name=name,
                                              body=body,
                                              future=future,
                                              uid=uid,
                                              headers=headers)
            return True
        
        self.rabbitmq.queue.bind(channel=self.rabbitmq_channel,
                                 queue=self.queue,
                                 exchange=options.tornpack_rabbitmq_exchange['headers_any'],
                                 routing_key='',
                                 arguments={'uid':uid},
                                 future=self.ioengine.future_instance(on_bind))
        return True
    
    def __publish(self,name,body,future,uid,headers):
        def on_encode(result):
            try:
                self.rabbitmq_channel.basic_publish(exchange=options.tornpack_rabbitmq_exchange['default'],
                                                    routing_key=routing_key(name),
                                                    body=result.result(),
                                                    properties=BasicProperties(headers=headers))
            except:
                future.set_result(False)
                raise
            else:
                self.futures[uid] = future
            return True

        self.__protobuf_encode__(body,self.ioengine.future_instance(on_encode))
        return True
    
    def on_message(self,msg):
        try:
            assert msg['properties'].headers['uid'] in self.futures
        except (AssertionError,KeyError):
            msg['ack'].set_result(False)
        except:
            raise
        else:
            self.futures[msg['properties'].headers['uid']].set_result(msg['body'])
            del self.futures[msg['properties'].headers['uid']]
            msg['ack'].set_result(True)
        return True

    def publish(self,name,body,future,uid,headers={}):
        def on_ready(result):
            self.ioengine.ioloop.add_callback(self.__bind,
                                              name=name,
                                              body=body,
                                              future=future,
                                              uid=uid,
                                              headers=headers)
            return True
        
        try:
            assert self.is_ready
        except AssertionError:
            self.promises.append(self.ioengine.future_instance(on_ready))
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(on_ready,result=True)
        return True

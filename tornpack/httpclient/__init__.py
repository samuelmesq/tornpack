# -*- coding: utf-8 -*-
from tornado.httpclient import AsyncHTTPClient,HTTPRequest
from tornado.httputil import HTTPHeaders
from tornpack.actor.base import Base

__all__ = ['HTTPClient']

class HTTPClient(Base):
    @property
    def schema(self):
        return ['url']

    @property
    def validation_ok(self):
        return self.payload
    
    def client(self,**kwargs):
        kwargs = kwargs['msg']
        kwargs['swap']['client'] = AsyncHTTPClient(force_instance=True)
        kwargs['swap']['client'].configure(None,max_clients=1)

        self.ioengine.ioloop.add_callback(self.fetch,msg=kwargs)
        return True
    
    def fetch(self,**kwargs):
        kwargs = kwargs['msg']

        def on_fetch(response):
            kwargs['swap']['future'] = response           
            self.ioengine.ioloop.add_callback(self.future,msg=kwargs)
            kwargs['swap']['client'].close()
            return True

        kwargs['swap']['client'].fetch(request=HTTPRequest(**kwargs['swap']['payload']),callback=on_fetch)
        return True
       
    def make(self,**kwargs):
        kwargs['swap'] = {'envelop':{}}
        self.ioengine.ioloop.add_callback(self.validate,msg=kwargs)
        return True

    def payload(self,**kwargs):
        kwargs = kwargs['msg']
        kwargs['swap']['payload'] = {'url':kwargs['msg']['url']}

        self.ioengine.ioloop.add_callback(self.payload_body,msg=kwargs)        
        return True

    def payload_auth(self,**kwargs):
        kwargs = kwargs['msg']

        try:
            assert 'auth_username' in kwargs['msg']
            assert 'auth_password' in kwargs['msg']
        except AssertionError:
            pass
        except:
            raise
        else:
            kwargs['swap']['payload']['auth_username'] = kwargs['msg']['auth_username']
            kwargs['swap']['payload']['auth_password'] = kwargs['msg']['auth_password']
        
        self.ioengine.ioloop.add_callback(self.payload_method,msg=kwargs)
        return True
    
    def payload_body(self,**kwargs):
        kwargs = kwargs['msg']

        try:
            assert 'body' in kwargs['msg']
        except AssertionError:
            pass
        except:
            raise
        else:
            kwargs['swap']['payload']['body'] = kwargs['msg']['body']

        self.ioengine.ioloop.add_callback(self.payload_auth,msg=kwargs)
        return True

    def payload_headers(self,**kwargs):
        kwargs = kwargs['msg']

        try:
            assert kwargs['msg']['headers']
        except (AssertionError,KeyError):
            pass
        except:
            raise
        else:
            kwargs['swap']['payload']['headers'] = HTTPHeaders(**kwargs['msg']['headers'])

        self.ioengine.ioloop.add_callback(self.payload_timeout_connect,msg=kwargs)
        return True
    
    def payload_method(self,**kwargs):
        kwargs = kwargs['msg']

        try:
            assert 'method' in kwargs['msg']
        except AssertionError:
            kwargs['swap']['payload']['method'] = 'GET'
        except:
            raise
        else:
            kwargs['swap']['payload']['method'] = kwargs['msg']['method']

        self.ioengine.ioloop.add_callback(self.payload_user_agent,msg=kwargs)
        return True

    def payload_timeout_connect(self,**kwargs):
        kwargs = kwargs['msg']

        try:
            assert kwargs['msg']['connect_timeout']
        except (AssertionError,KeyError):
            kwargs['swap']['payload']['connect_timeout'] = 2
        except:
            raise
        else:
            kwargs['swap']['payload']['connect_timeout'] = kwargs['msg']['connect_timeout']

        self.ioengine.ioloop.add_callback(self.payload_timeout_request,msg=kwargs)
        return True

    def payload_timeout_request(self,**kwargs):
        kwargs = kwargs['msg']

        try:
            assert kwargs['msg']['request_timeout']
        except (AssertionError,KeyError):
            kwargs['swap']['payload']['request_timeout'] = 10
        except:
            raise
        else:
            kwargs['swap']['payload']['request_timeout'] = kwargs['msg']['request_timeout']

        self.ioengine.ioloop.add_callback(self.client,msg=kwargs)
        return True
    
    def payload_user_agent(self,**kwargs):
        kwargs = kwargs['msg']

        try:
            assert 'user_agent' in kwargs['msg']
        except AssertionError:
            kwargs['swap']['payload']['user_agent'] = 'Tornpack'
        except:
            raise
        else:
            kwargs['swap']['payload']['user_agent'] = kwargs['msg']['user_agent']

        self.ioengine.ioloop.add_callback(self.payload_headers,msg=kwargs)
        return True

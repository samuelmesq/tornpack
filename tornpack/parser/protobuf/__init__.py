from base64 import b64decode,b64encode

try:
    from protobuf_to_dict import protobuf_to_dict as pb2d
except:
    pb2d = None

__all__ = ['decode','encode','pb2d']

def decode(msg,proto):
    proto.ParseFromString(b64decode(msg))
    return proto
        
def encode(proto):
    return b64encode(proto.SerializeToString())

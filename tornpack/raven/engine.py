import logging
from tornpack.actor.base import Base

__all__ = ['Engine']

class Engine(Base):
    __logger = None

    @property
    def logger(self):
        try:
            assert self.__logger
        except AssertionError:
            self.__logger = logging.getLogger("tornpack")
            self.__logger.setLevel(logging.INFO)
        except:
            raise
        return self.__logger

    def critical(self,arg):
        self.logger.critical(arg)
        return True

    def debug(self,arg):
        self.logger.debug(arg)
        return True

    def error(self,arg):
        self.logger.error(arg)
        return True

    def info(self,arg):
        self.logger.info(arg)        
        return True
    
    def warning(self,arg):
        self.logger.warning(arg)
        return True

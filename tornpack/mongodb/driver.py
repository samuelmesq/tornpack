from motor import MotorClient
from tornpack.actor.base import Base

__all__ = ['Driver']

class Driver(Base):    
    def connect(self,url,future,replicaSet=False):
        def on_connect(result,error):
            try:
                assert result
            except AssertionError:
                self.ioengine.ioloop.add_callback(self.future,
                                                  msg={'future':future,
                                                       'swap':{'future':False}})
                raise error
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(self.future,
                                                  msg={'future':future,
                                                       'swap':{'future':result}})
            return True

        try:
            assert replicaSet
        except AssertionError:
            MotorClient(url).open(callback=on_connect)
        except:
            raise
        else:
            pass
        return True

Driver = Driver()

from tornpack.actor.base import Base

__all__ = ['Client']

class Client(Base):
    def __init__(self,db,collection,io):
        self.collection = collection
        self.db = db
        self.is_ready = io

    def aggregate(self,pipeline,future):
        def on_aggregate(result,error):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result}})
            return True

        self.io[self.db][self.collection].aggregate(pipeline,callback=on_aggregate)
        return True

    def distinct(self,field,future):
        def on_distinct(result,error):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result}})
            return True

        self.io[self.db][self.collection].distinct(field,callback=on_distinct)
        return True

    def count(self,document,future):
        def on_count(result,error):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result}})
            return True

        self.io[self.db][self.collection].find(document).count(callback=on_count)
        return True

    def cursor(self,document,future,**kwargs):
        self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                           'swap':{'future':self.io[self.db][self.collection].find(document,**kwargs)}})
        return True

    def find(self,document,on_each,**kwargs):
        self.io[self.db][self.collection].find(document,**kwargs).each(on_each)
        return True

    def find_and_modify(self,query,document,future,**kwargs):
        def on_find(result,error):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result}})
            return True

        self.io[self.db][self.collection].find_and_modify(query,document,callback=on_find,**kwargs)
        return True

    def find_one(self,document,future,**kwargs):
        def on_find(result,error):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result}})
            return True

        self.io[self.db][self.collection].find_one(document,callback=on_find,**kwargs)
        return True

    def insert(self,document,future=None):
        def on_store(result,error):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result}})
            return True

        self.io[self.db][self.collection].insert(document,callback=on_store)
        return True

    def remove(self,document,future=None,**kwargs):
        def on_remove(result,error):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result}})
            return True

        self.io[self.db][self.collection].remove(document,callback=on_remove,**kwargs)
        return True

    def save(self,document,future=None):
        def on_save(result,error):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result}})
            return True

        self.io[self.db][self.collection].save(document,callback=on_save)
        return True

    def update(self,query,document,future=None,**kwargs):
        def on_update(result,error):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,
                                                               'swap':{'future':result}})
            return True

        self.io[self.db][self.collection].update(query,document,callback=on_update,**kwargs)
        return True

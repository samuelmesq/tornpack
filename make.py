from argparse import ArgumentParser
from uuid import uuid4

class TornpackSettings(object):
    __file = None
    __parser = None

    @property
    def file(self):
        try:
            assert self.__file
        except AssertionError:
            self.__file = open('tornpack_settings.py','w')
        except:
            raise
        return self.__file

    @property
    def parser(self):
        try:
            assert self.__parser
        except AssertionError:
            self.__parser = ArgumentParser(prog='make',description='''Create Tornpack's settings''')
        except:
            raise
        return self.__parser
        
    @property
    def uuid(self):
        return uuid4().hex

    def __init__(self):
        self.parser_setup()
        self.routine()

    def parser_setup(self):
        self.parser.add_argument('-n','--name',default=self.uuid,dest='name')
        self.parser.add_argument('-r','--raid',default=False,dest='raid')
        self.parser.add_argument('-rp','--raid_port',default=8881,dest='raid_port')
        self.parser.add_argument('-s','--services',default=None)
        return True

    def routine(self):
        self.tornado_options()
        self.tornpack_name()
        self.tornpack_raid()
        self.tornpack_services()
        self.file.close()
        return True

    def tornado_options(self):
        self.file.write('''from tornado.options import define,options\n''')
        return True

    def tornpack_name(self):
        self.file.write('''define("tornpack_name",default="%s")\n''' % self.parser.parse_args().name)
        return True

    def tornpack_raid(self):
        self.file.write('''define("tornpack_raid",default=%s)\n''' % self.parser.parse_args().raid)
        self.file.write('''define("tornpack_raid_port",default=%s)\n''' % self.parser.parse_args().raid_port)
        return True
    
    def tornpack_services(self):
        self.file.write('''define("tornpack_services",default=%s)\n''' % self.parser.parse_args().services)
        return True

if __name__ == "__main__":
    TornpackSettings()

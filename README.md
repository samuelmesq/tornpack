# Tornpack

https://gitlab.com/vikingmakt/tornpack

+ [Defini&ccedil;&otilde;es](#defini%C3%A7%C3%B5es)
+ [Actors](#actors)
    + [Base](#base)
    + [HTTP](#http)
    + [MDB](#mdb)
    + [RabbitMQ](#rabbitmq)
        + [Channel](#channel)
        + [Connection](#connection)
        + [Exchange](#exchange)
        + [Queue](#queue)
        + [SimpleAsk](#simpleask)
        + [SimpleBinder](#simplebinder)
        + [SimpleConsumer](#SimpleConsumer)
        + [SimplePublisher](#SimplePublisher)
+ [DHTDB](#dhtdb)
+ [HTTPClient](#httpclient)
+ [HTTPD](#httpd)
+ [IOEngine](#ioengine)
+ [Mongodb](#mongodb)
+ [Njord](#njord)
+ [Options](#options)
+ [Parser](#parser)
    + [Date](#date)
    + [Json](#json)
    + [JWT](#jwt)
    + [Protobuf](#protobuf)
+ [Promise](#promise)
+ [RabbitMQ](#rabbitmq-1)
    + [Consumer](#consumer)
    + [Exchange](#exchange-1)
    + [Queue](#queue-1)
    + [Publish](#publish)
+ [RAID](#raid)
	+ [HTTP](#http-1)
	+ [Kernel](#kernel)
+ [Raven](#raven)
+ [Swarm](#swarm)
+ [Refer&ecirc;ncias](#refer%C3%AAncias)

# Defini&ccedil;&otilde;es

+ **WIP** - (i.e.: Work In Progress) Em desenvolvimento
+ **hash** - String gerada a partir de uma f&oacute;rmula ou com valores aleat&oacute;rios
+ **kwargs** - (i.e.: Key with Args) Par&acirc;metros passados a fun&ccedil;&otilde;es com nome, que n&atilde;o est&atilde;o definidas nos par&acirc;metros aceitos da fun&ccedil;&atilde;o, ficar&atilde;o armazenados como um **dict** dentro do **kwargs**, sendo o chave o nome do par&acirc;metro e valor o valor do par&acirc;metro. Sua sintaxe fica como: **kwargs
+ **uuid4** - (i.e.: Universally Unique IDentifier version 4) Identificador &uacute;nico Universal vers&atilde;o 4, gera **hashs** aleat&oacute;rios, ver [UUID](http://www.ietf.org/rfc/rfc4122.txt)
+ **kernel** - Pode ser traduzido para "gerenciador", controla todos as opera&ccedil;&otilde;es das funcionalidades que fazem parte dele
+ **Json** - (i.e.: JavaScript Object Notation) Formato de mensagem para serializa&ccedil;&atilde;o de derivados de objetos **JavaScript**, ver [JSON](http://www.rfc-editor.org/rfc/rfc7159.txt)
+ **JWT** - (i.e.: JSON Web Token) Formato de mensagem encriptado via um token, que somente pode ser desencriptado pelo mesmo token, ver [JWT](https://tools.ietf.org/html/rfc7519)
+ **dict** - Conjunto de chave e valor (i.e.: **Map**), a chave deve ser do tipo **string** e o valor pode ser de qualquer tipo, ver [dict](https://docs.python.org/2/tutorial/datastructures.html#dictionaries)
+ **decorator**: M&eacute;todo que &eacute; executado antes de executar o m&eacute;todo chamado, ver [decorator](https://wiki.python.org/moin/PythonDecorators)
+ [datetime](https://docs.python.org/2/library/datetime.html#datetime.datetime)
+ [timedelta](https://docs.python.org/2/library/datetime.html#datetime.timedelta)
+ [Unix time](https://en.wikipedia.org/wiki/Unix_time)
+ Todo actor deve herdar as classes [Actors](#actors)
+ [Futuro != Promessa](https://en.wikipedia.org/wiki/Futures_and_promises)

# Actors

```
- tornpack
 - actor
  - base
   Base
  - http
   HTTP
  MDB
  - rabbitmq
   Channel
   Connection
   Exchange
   Queue
   SimpleAsk
   SimpleBinder
   SimpleConsumer
   SimplePublisher
```

## Base

Base para os actors

Exemplo:

```python
from tornpack.actor.base import Base

class ExampleClass(Base):
    ...
```

### Atributos

+ **ioengine**: Traz um instancia de [IOEngine](#ioengine)

+ **promises**: Lista de promessas esperando para serem completas

+ **is_ready**:
 - get: Traz o valor atribuido
 - set: Atribui o valor e completa as promessas que est&atilde;o em **promises**

+ **uid**: id &uacute;nico do actor

### M&eacute;todos

+ **run**: Recebe kwargs e chama via IOLoop callback o m&eacute;todo **make**
+ **make**: (Overwrite) Deve ser sobreescrito para ser usado com o m&eacute;todo **run**

## HTTP

*Handler* HTTP
+ Usar o [decorator](https://wiki.python.org/moin/PythonDecorators) **async** para tornar o chamada ao *handler* ass&iacute;crona

```
- tornpack
 - actor
  - http
   async
   HTTP
```

Exemplo:

```python
from tornpack.actor.http import HTTP,async

class Handler(HTTP):
    @async
    def get(self):
        self.write('Hello')
        self.finish()
```

### Atributos

+ **etag**: *Hash* &uacute;nico para a requisi&ccedil;&atilde;o

### M&eacute;todos

+ **\_\_on_finish__**: Chamado ap&oacute;s a requisi&ccedil;&atilde;o for completada
+ **\_\_prepare__**: Chamado antes da requisi&ccedil;&atilde;o ser iniciada
+ **get**, **head**, **post**, **delete**, **patch**, **put**, **options**: (Overwrite) M&eacute;todos que devem ser sobreescritos conforme os m&eacute;todos HTTP aceitos

## MDB

Actor para utilizar o Mongodb

### M&eacute;todos

+ **db**: Abre um [Client](#client) Mongodb, sendo todos os par&acirc;metros obrigat&oacute;rios
 - **name**: Nome da conex&atilde;o Mongodb
 - **db**: Nome da *database*
 - **collection**: Nome da *collection*
 - **future**: Futuro que ser&aacute; completado com o [Client](#client) Mongodb

### Client

#### M&eacute;todos

+ **aggregate**: *Map-reduce*
 - **pipeline**: Conjunto de opera&ccedil;&otilde;es para o map-reduce, ver [operadores](https://docs.mongodb.org/manual/reference/operator/aggregation)
 - **future**: Futuro que ser&aacute; completado com um objeto **MotorCommandCursor**, semelhante ao [MotorCursor](http://motor.readthedocs.org/en/stable/api/motor_cursor.html#motor.motor_tornado.MotorCursor)
 - [Todos os par&acirc;metros](http://motor.readthedocs.org/en/stable/api/motor_collection.html?motor.motor_tornado.MotorCollection.aggregate)

+ **distinct**: Traz todas as possibilidades de valores de um **field** espec&iacute;fico
 - **field**: Chave que os valores ser&atilde;o buscados
 - **future**: Futuro que ser&aacute; completado com uma lista com todos os valores encontrados
 - [Todos os par&acirc;metros](http://motor.readthedocs.org/en/stable/api/motor_collection.html?motor.motor_tornado.MotorCollection.distinct)

+ **count**: Faz a contagem de documentos que correspondem ao **document** (Evitar usar este m&eacute;todo por ser uma opera&ccedil;&atilde;o muito custosa ao Mongodb)
 - **document**: Ver [operadores](https://docs.mongodb.org/manual/reference/operator/query)
 - **future**: Futuro que ser&aacute; completado com o resultado da quantidade
 - [Todos os par&acirc;metros](http://motor.readthedocs.org/en/stable/api/motor_collection.html?motor.motor_tornado.MotorCollection.count)

+ **cursor**: Cria um [MotorCursor](http://motor.readthedocs.org/en/stable/api/motor_cursor.html#motor.motor_tornado.MotorCursor)
 - **document**: Ver [operadores](https://docs.mongodb.org/manual/reference/operator/query)
 - **future**: Futuro que ser&aacute; completado com o **MotorCursor**
 - [Todos os par&acirc;metros](http://motor.readthedocs.org/en/stable/api/motor_collection.html?motor.motor_tornado.MotorCollection.find)

+ **find**: Executa a *query* e chama o *callback* para cada resultado
 - **document**: Ver [operadores](https://docs.mongodb.org/manual/reference/operator/query)
 - **on_each**: *Callback* que deve ser chamado a cada resultado, sendo que quando n&atilde;o ouver mais resultados retornar&aacute; **None**
 - [Todos os par&acirc;metros](http://motor.readthedocs.org/en/stable/api/motor_collection.html?motor.motor_tornado.MotorCollection.find)

+ **find_and_modify**: Atualiza e retorna um documento
 - **query**: Ver [operadores](https://docs.mongodb.org/manual/reference/operator/query)
 - **document**: Altera&ccedil;&atilde;o do documento, ver [operadores](https://docs.mongodb.org/manual/reference/operator/update)
 - **future**: Futuro que ser&aacute; completado com o documento resultante
 - [Todos os par&acirc;metros](http://motor.readthedocs.org/en/stable/api/motor_collection.html?motor.motor_tornado.MotorCollection.find_and_modify)

+ **find_one**: Busca e retorna um documento que corresponda ao **document**
 - **document**: Ver [operadores](https://docs.mongodb.org/manual/reference/operator/query)
 - **future**: Futuro que ser&aacute; completado com o documento encontrado
 - [Todos os par&acirc;metros](http://motor.readthedocs.org/en/stable/api/motor_collection.html?motor.motor_tornado.MotorCollection.find_one)

+ **insert**: Insere um documento ou uma lista de documentos
 - **document**: Documento a ser inserido
 - **future**: Futuro que ser&aacute; completado com o **id** do documento
 - [Todos os par&acirc;metros](http://motor.readthedocs.org/en/stable/api/motor_collection.html?motor.motor_tornado.MotorCollection.insert)

+ **remove**: Remove um ou mais documentos
 - **document**: Ver [operadores](https://docs.mongodb.org/manual/reference/operator/query)
 - **future**: Futuro que ser&aacute; completado com o resultado da opera&ccedil;&atilde;o
 - [Todos os par&acirc;metros](http://motor.readthedocs.org/en/stable/api/motor_collection.html?motor.motor_tornado.MotorCollection.remove)

+ **save**: Atualiza um documento pelo **_id** e caso n&atilde;o exista, cria o documento
 - **document**: Documento a ser atualizado
 - **future**: Futuro que ser&aacute; completado com o **id** do documento
 - [Todos os par&acirc;metros](http://motor.readthedocs.org/en/stable/api/motor_collection.html?motor.motor_tornado.MotorCollection.save)

+ **update**: Busca e atualiza um documento
 - **query**: Ver [operadores](https://docs.mongodb.org/manual/reference/operator/query)
 - **document**: Altera&ccedil;&atilde;o do documento, ver [operadores](https://docs.mongodb.org/manual/reference/operator/update)
 - **future**: Futuro que ser&aacute; completado com a descri&ccedil;&atilde;o da opera&ccedil;&atilde;o
 - [Todos os par&acirc;metros](http://motor.readthedocs.org/en/stable/api/motor_collection.html?motor.motor_tornado.MotorCollection.update)

## RabbitMQ

### static

Encripta o nome do **channel**, fila e rota

```
- tornpack
 - actor
  - rabbitmq
   - static
    channel_name
	queue_name
	routing_key
```

Exemplo:

```python
> from tornpack.actor.rabbitmq.static import routing_key

> routing_key('actor_name')
'tornpack.actor.rmq.d452a7040f3c08451dc9c2f3ba92b91c927b88d6'
```

+ **channel_name**:
 - **name**: Nome do actor
 - **uid**: **uuid4** do **actor**

+ **queue_name**:
 - **_**: Nome do actor

+ **routing_key**:
 - **_**: Nome do actor

### Channel

Actor respons&aacute;vel por abrir e fechar *channels*

```
- tornpack
 - actor
  - rabbitmq
   - channel
    Channel
```

Exemplo:

```python
from tornpack.actor.rabbitmq.channel import Channel

class ExampleClass(Channel):
    def __init__(self,name):
	    self.name = name
        self.ioengine.ioloop.add_callback(self.__rabbitmq_channel__)
```

#### Atributos

+ **name**:
 - get: Retorna o nome actor
 - set: Define o nome do actor

+ **rabbitmq_channel**: Retorna um objeto **channel** caso criado, caso contr&aacute;rio retorna **None**

+ **rabbitmq_channel_prefetch_count**: Retorna o **prefetch_count** do **channel**

+ **rabbitmq_channel_name**: Gera o nome do **channel**

#### Hooks

+ **\_\_on_rabbitmq_channel__**: Chamado assim que um **channel** for aberto, deve receber 1 par&acirc;metro
 - **channel**: Objeto **channel**

#### M&eacute;todos

+ **__rabbitmq_channel__**: Abre um novo **channel**
 - **future**: Futuro que ser&aacute; completado com o **channel** aberto

+ **__rabbitmq_channel_close__**: Fecha o **channel**

### Connection

Actor respons&aacute;vel por abrir e fechar conex&otilde;es

```
- tornpack
 - actor
  - rabbitmq
   - connection
    Connection
```

Exemplo:

```python
from tornpack.actor.rabbitmq.connection import Connection

class ExampleClass(Connection):
    ...
```

#### Atributos

+ **rabbitmq_connection**: Retorna a conex&atilde;o caso conectado, caso contr&aacute;rio retorna **None**

#### Hooks

+ **\_\_on_rabbitmq_connection__**: Chamado assim que uma conex&atilde;o for aberta, deve receber 1 par&acirc;metro
 - **connection**: Objeto **Engine**

#### M&eacute;todos

+ **\_\_rabbitmq_connection__**: Abre uma nova conex&atilde;o
 - **future**: Futuro que ser&aacute; completado com o objeto **Engine** e a conex&atilde;o aberta

### Exchange

Actor respons&aacute;vel por declarar *exchanges*

```
- tornpack
 - actor
  - rabbitmq
   - exchange
    Exchange
```

Exemplo:

```python
from tornpack.actor.rabbitmq.exchange import Exchange

class ExampleClass(Exchange):
    ...
```

#### Hooks

+ **\_\_on_rabbitmq_exchange_declare__**: Chamado assim que uma *exchange* for criada, deve receber 1 par&acirc;metro
 - **exchange**: Nome da *exchange*

#### M&eacute;todos

+ **\_\_rabbitmq_exchange_declare__**: Cria uma nova *exchange*, sendo **exchange**, **exchange_type**, **future** obrigat&oacute;rios
 - **exchange**: Nome da *exchange*
 - **exchange_type**: Tipo da *exchange*
 - **future**: Futuro que ser&aacute; completado com o nome da *exchange*
 - **passive**: Se somente far&aacute; a checagem que a *exchange* existe (valor padr&atilde;o **False**)
 - **durable**: Se a *exchange* &eacute; duravel (valor padr&atilde;o **False**)
 - **auto_delete**: Se a *exchange* ser&aacute; deletada automaticamente (valor padr&atilde;o **False**)
 - **internal**: Se a *exchange* &eacute; interna (valor padr&atilde;o **False**)
 - **arguments**: Argumentos da *exchange*

### Queue

Actor respons&aacute;vel por declarar e vincular filas

```
- tornpack
 - actor
  - rabbitmq
   - queue
    Queue
```

Exemplo:

```python
from tornpack.actor.rabbitmq.queue import Queue

class ExampleClass(Queue):
    ...
```

#### Hooks

+ **\_\_on_rabbitmq_queue_bind__**: Chamado assim que uma fila for vinculada, deve receber 3 par&acirc;metros
 - **queue**: Nome da fila
 - **exchange**: Nome da *exchange*
 - **routing_key**: Rota do v&iacute;nculo

+ **\_\_on_rabbitmq_queue_declare__**: Chamado assim que uma fila for criada, deve receber 1 par&acirc;metro
 - **queue**: Nome da fila

#### M&eacute;todos

+ **\_\_rabbitmq_queue_bind__**: Vincula uma fila a uma *exchange*
 - **queue**: Nome da fila
 - **exchange**: Nome da *exchange*
 - **routing_key**: Rota para a fila
 - **arguments**: *Headers* para a fila
 - **future**: Futuro que ser&aacute; completado quando o v&iacute;nculo for feito

+ **\_\_rabbitmq_queue_declare__**: Cria uma fila
 - **queue**: Nome da fila
 - **passive**: Se somente far&aacute; a checagem que a fila existe (valor padr&atilde;o **False**)
 - **durable**: Se a fila &eacute; duravel (valor padr&atilde;o **False**)
 - **exclusive**: Se a fila somente aceitar&aacute; 1 consumidor por vez (valor padr&atilde;o **False**)
 - **auto_delete**: Se a fila ser&aacute; deletada automaticamente (valor padr&atilde;o **False**)
 - **nowait**: Aguarda a opera&ccedil;&atilde;o no RabbitMQ ser conclu&iacute;da para chamar o futuro (valor padr&atilde;o **False**)
 - **arguments**: Argumentos da fila
 - **future**: Futuro que ser&aacute; completado quando a fila for declarada

+ **__rabbitmq_queue_name__**: Chama a fun&ccedil;&atilde;o [static.queue_name](#static)
 - **_**: Nome do actor

+ **__rabbitmq_queue_name__**: Chama a fun&ccedil;&atilde;o [static.routing_key](#static)
 - **_**: Nome do actor

### SimpleAsk

Actor que envia uma mensagem pelo RabbitMQ e recebe a resposta da opera&ccedil;&atilde;o

#### Construtor

+ **etag**: **Hash** &uacute;nico para identifica&ccedil;&atilde;o da mensagem, obrigat&oacute;rio
+ **future**: Futuro que ser&aacute; completado com o resultado da opera&ccedil;&atilde;o, obrigat&oacute;rio
+ **body**: Corpo da mensagem (valor padr&atilde;o **''**)
+ **exchange**: *Exchange* que a mensagem ser&aacute; enviada (valor padr&atilde;o **tornpack**)
+ **routing_key**: Rota com que a mensagem ser&aacute; enviada (valor padr&atilde;o **''**)
+ **headers**: *Headers* com que a mensagem ser&aacute; enviada

### SimpleBinder

Actor para binds no RabbitMQ

#### M&eacute;todos

+ **__rabbitmq_queue_bind__**: Faz o bind da queue com a exchange
 - exchange: Nome da exchange
 - headers: Headers do bind
 - future: Futuro que sera completado assim que o bind for feito
 - queue: Nome da queue
 - routing_key: Rota do bind

+ **__rabbitmq_queue_unbind__**: Desfaz o bind da queue com a exchange
 - exchange: Nome da exchange
 - headers: Headers do bind
 - future: Futuro que sera completado assim que o unbind for feito
 - queue: Nome da queue
 - routing_key: Rota do bind

### SimpleConsumer

Actor consumidor do RabbitMQ
+ O actor deve receber um nome &uacute;nico no sistema inteiro e que outros tenham conhecimento deste nome
+ Quando instanciado cria 3 exchanges:
 - **tornpack** do tipo **topic**
 - **tornpack_headers_any** do tipo **headers** que ouve qualquer mensagem que chegue a exchange **tornpack**
 - **tornpack_headers** do tipo **headers** que ouve qualquer mensagem que chegue a exchange **tornpack** e com o argumento *match-all*
+ Quando instanciado cria a pr&oacute;pria *queue* e faz o bind na exchange **tornpack** com a *routing_key* baseada no nome

#### Construtor

+ **name**: Nome do consumer, obrigat&oacute;rio
+ **prefetch_count**: Mensagens consumidas por vez (valor padrao **10**)
+ **queue_arguments**: Argumentos para a queue, ver [Docs](https://pika.readthedocs.org/en/0.10.0/modules/channel.html#pika.channel.Channel.queue_declare) (valor padrao **exclusive**, **durable**)

#### M&eacute;todos

+ **on_message**: (Overwrite) M&eacute;todo que recebe **msg** assim que uma mensagem chegar a fila do RabbitMQ
    - **msg**:
        - **channel**: Channel do consumer
        - **deliver**: RabbitMQ deliver
        - **properties**: Propriedades da mensagem
        - **body**: Corpo da mensagem
        - **ack**: Futuro que quando completado com *True* a mensagem &eacute; removida da fila, e com *False* ela retorna a fila

### SimplePublisher

Actor para publish do RabbitMQ

#### M&eacute;todos

+ **\_\_publish__**: Envia a mensagem ao RabbitMQ
 - **body**: Corpo da mensagem
 - **future**: Futuro completado assim que a mensagem for enviada
 - **exchange**: Exchange para o publish (valor padr&atilde;o **tornpack**)
 - **routing_key**: Rota para a mensagem (valor padr&atilde;o **''**)
 - **properties**: Objeto [BasicProperties](https://pika.readthedocs.org/en/0.10.0/modules/spec.html#pika.spec.BasicProperties)

# DHTDB

**WIP**

# HTTPClient

Faz requisi&ccedil;&otilde;es ass&iacute;ncronas via HTTP

```
- tornpack
 - httpclient
  HTTPClient
```

Exemplo:

```python
from tornpack.httpclient import HTTPClient

def on_response(response):
    ...

HTTPClient().run(
    msg={'url': 'http://localhost/'},
    future=self.ioengine.future_instance(on_response)
)
```

## M&eacute;todos

+ **run**: Recebe **kwargs** sendo a chave **url** obrigat&oacute;ria
 - **url**: String da url
 - **body**: String do corpo da request
 - **auth_username**: Usu&aacute;rio para autentica&ccedil;&atilde;o
 - **auth_password**: Senha para autentica&ccedil;&atilde;o
 - **method**: M&eacute;todo do HTTP (**GET**, **HEAD**, **POST**, **DELETE**, **PATCH**, **PUT**, **OPTIONS**) (valor padr&atilde;o **GET**)
 - **user_agent**: *user_agent* (valor padr&atilde;o **Tornpack**)
 - **headers**: **Dict** de headers
 - **connect_timeout**: Limite de tempo em segundos para a conex&atilde;o ser feita (valor padr&atilde;o **2**)
 - **request_timeout**: Limite de tempo em segundos para a requisi&ccedil;&atilde;o ser completada (valor padr&atilde;o **10**)
 - **future**: Futuro que ser&aacute; completado com o resultado o objeto [HTTPResponse](http://www.tornadoweb.org/en/stable/httpclient.html#tornado.httpclient.HTTPResponse)

# HTTPD

Servidor HTTP

```
- tornpack
 - httpd
  Kernel
```

Exemplo:

```python
from tornpack.httpd import Kernel

def on_start(result):
    ...

def on_server(server):
    server.result().add_handler(
        handlers=[
            (r'/test', TestHandler)
        ]
    )
    server.result().start(self.ioengine.future_instance(on_start),8088)
    return True

Kernel.server('my_server',self.ioengine.future_instance(on_server))
```

## M&eacute;todos

+ **server**: Inicia uma instancia do engine HTTPD
 - **name**: Nome HTTP server
 - **future**: Futuro que ser&aacute; completado com uma instancia do engine HTTPD

## Engine

### M&eacute;todos

+ **add_handler**: Define os *handlers* que ser&atilde;o chamados pelo servidor
 - **pattern**: Virtual host *pattern*
 - **handlers**: Lista de tuplas sendo o primeiro valor a **uri** e o segundo um actor do tipo [HTTP](#http)
 - **future**: Futuro que ser&aacute; completado quando os *handlers* forem definidos

+ **start**: Inicia o servidor HTTPD sendo **future** obrigat&oacute;rio
 - **future**: Futuro que ser&aacute; completado quando o servidor for iniciado
 - **port**: N&uacute;mero da porta que o servidor ir&aacute; ouvir

# IOEngine

Abstra&ccedil;&atilde;o do IOLoop + Futuro + Rotinas

```
- tornpack
 - ioengine
  IOEngine
```

## Attributos

+ **ioloop**: Retorna uma inst&acirc;ncia de [IOLoop](http://www.tornadoweb.org/en/stable/ioloop.html#tornado.ioloop.IOLoop)

+ **uuid4**: Retorna o hexa-decimal de um **uuid4**

+ **routines**: **Dict** de rotinas em execu&ccedil;&atilde;o, sendo a chave o **uuid4** e valor a rotina

+ **add_routine**: Adiciona uma nova rotina, sendo todos os par&acirc;metros obrigat&oacute;rios
 - **time**: Tempo em **Unix time** ou **timedelta** que o evento deve ser disparado
 - **event**: *Callback* que ser&aacute; chamado toda vez que o evento for disparado

+ **future_instance**: Recebe um *callback* e retorna um futuro com este *callback*
 - **actor**: *Callback* que ser&aacute; executado quando o futuro for completado

# Mongodb

Mongo **kernel**

```
- tornpack
 - mongodb
  Mongodb
```

Exemplo:

```python
def on_connected(result):
    ...

Mongodb.engine_open(
    name='mongo_connection',
    url='mongodb://localhost',
    future=self.ioengine.future_instance(on_connected))
```

## M&eacute;todos

+ **engine**: Cria um novo **Engine** do Mongodb
 - **name**: Nome do **Engine**
 - **future**: Futuro que ser&aacute; completado com o **Engine**, n&atilde;o conectado

+ **engine_open**: Cria um novo **Engine** do Mongodb, sendo **name**, **url** obrigat&oacute;rios
 - **name**: Nome do **Engine**
 - **url**: Url do *cluster* Mongo
 - **future**: Futuro que ser&aacute; completado com o **Engine**, j&aacute; conectado

## Engine

### M&eacute;todos

+ **client**: Retorna um [Client](#client) quando conectado
 - **db**: Nome da *database*
 - **collection**: Nome da *collection*
 - **future**: Futuro que ser&aacute; completado com um objeto [Client](#client)

+ **connect**: Abre a conex&atilde;o com o Mongo
 - **url**: Url do *cluster* Mongo

+ **connection**: Completa os futuros assim que o conectado com o Mongo
 - **future**: Futuro que ser&aacute; completado com o pr&oacute;prio objeto **Engine**

# Njord

Envia uma mensagem ao **Njord HTTP** para executar uma requisi&ccedil;&atilde;o

## M&eacute;todos

+ **run**: Executa a requisi&ccedil;&atilde;o HTTP
 - **url**: Url da requisi&ccedil;&atilde;o
 - **future**: Futuro que ser&aacute; completado quando a requisi&ccedil;&atilde;o for completada
 - **headers**: **Dict** de *headers* da requisi&ccedil;&atilde;o
 - **method**: M&eacute;todo da requisi&ccedil;&atilde;o (e.g.: **GET**, **POST**)
 - **body**: Corpo da requisi&ccedil;&atilde;o
 - **auth_username**: Usu&aacute;rio para autentica&ccedil;&atilde;o
 - **auth_password**: Senha para autentica&ccedil;&atilde;o

# Options

Abstra&ccedil;&atilde;o do [options](http://www.tornadoweb.org/en/stable/options.html#module-tornado.options) do Tornado

## Fun&ccedil;&otilde;es disponiveis

+ **define**
+ **options**
+ **parse_config_file**
+ **parse_command_line**

# Parser

## Date

Conversor de data

```
- tornpack
 - parser
  - date
   datetime2timestamp
   timestamp2datetime
```

Exemplo:

```python
> from tornpack.parser.date import datetime2timestamp,timestamp2datetime

> datetime2timestamp(datetime.datetime(2016, 2, 11, 19, 9, 36, 644782))
1455217776.644782

> timestamp2datetime(1455217776.644782)
datetime.datetime(2016, 2, 11, 19, 9, 36, 644782)
```

### M&eacute;todos

+ **datetime2timestamp**: Converte de **datetime** para **Unix time**

+ **timestamp2datetime**: Converte de **Unix time** para **datetime**

## Json

Conversor de **JSON**

```
- tornpack
 - parser
  - json
   dictfy
   jsonify
```

Exemplo:

```python
> from tornpack.parser.json import dictfy,jsonify

> jsonify({'a':2,'b':3})
'{"a":2,"b":3}'

> dictfy('{"a":2,"b":3}')
{'a':2,'b':3}
```

### M&eacute;todos

+ **dictfy**: Converte um **JSON** em um **dict**

+ **jsonify**: Converte um **dict** em um **JSON**

## JWT

Conversor de **JWT**

```
- tornpack
 - parser
  - jwt
   decode
   encode
```

Exemplo:

```python
> from tornpack.parser.jwt import decode,encode

> encode({'a':2,'b':3},'2345654323')
'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhIjoyLCJiIjozfQ.8mp1UbA6CTp7z-RRDwDmFu8X8JDAlvDxIydFN36zeJ4'

> decode('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhIjoyLCJiIjozfQ.8mp1UbA6CTp7z-RRDwDmFu8X8JDAlvDxIydFN36zeJ4','2345654323')
{'a':2,'b':3}
```

### M&eacute;todos

+ **decode**: Converte a mensagem **JWT** em **dict**
 - **payload**: Mensagem codificada
 - **secret**: Segredo que ser&aacute; usado para decodificar a mensagem

+ **encode**: Converte um **dict** em mensagem **JWT**
 - **payload**: Mensagem a ser codificada
 - **secret**: Segredo que ser&aacute; usado para codificar a mensagem

## Protobuf

Conversor de objeto **Protobuf**

```
- tornpack
 - parser
  - protobuf
   decode
   encode
```

Exemplo:

```python
> from tornpack.parser.protobuf import decode,encode

> pb = Msg(uid='1234',url='http://localhost/',method=0)

> encode(pb)
'CgQxMjM0EhFodHRwOi8vbG9jYWxob3N0LxgA'

> result = Msg()
> decode('CgQxMjM0EhFodHRwOi8vbG9jYWxob3N0LxgA',result)

> result.url
'http://localhost/'
```

### M&eacute;todos

+ **decode**: Popula o objeto **Protobuf** com a mensagem
 - **msg**: Mensagem codificada
 - **proto**: Objeto **Protobuf** vazio

+ **encode**: Converte o objeto **Protobuf** em mensagem
 - **proto**: Objeto **Protobuf**

# Promise

**WIP**

# RabbitMQ

RabbitMQ **kernel**

```
- tornpack
 - rabbitmq
  RabbitMQ
```

## Atributos

+ **engines**: **Dict** sendo chave o nome e valor o objeto **Engine**

## M&eacute;todos

+ **engine_close**: Fecha o **Engine**, sendo **name** obrigat&oacute;rio
 - **name**: Nome do **Engine**
 - **future**: Futuro que ser&aacute; completado quando a **Engine** for fechada

+ **engine**: Cria um novo **Engine** do RabbitMQ
 - **name**: Nome do **Engine**
 - **future**: Futuro que ser&aacute; completado com o **Engine**, n&atilde;o conectado

+ **engine_open**: Cria um novo **Engine** do RabbitMQ, sendo **name**, **url** obrigat&oacute;rios
 - **name**: Nome do **Engine**
 - **url**: Url do *cluster* RabbitMQ
 - **future**: Futuro que ser&aacute; completado com o **Engine**, j&aacute; conectado

## Engine

### Atributos

+ **channels**: **Dict** sendo chave o nome e valor o objeto **Channel**

+ **consumer**: Retorna a inst&acirc;ncia est&aacute;tica do **Consumer**

+ **exchange**: Retorna a inst&acirc;ncia est&aacute;tica do **Exchange**

+ **queue**: Retorna a inst&acirc;ncia est&aacute;tica do **Queue**

+ **publish**: Retorna a inst&acirc;ncia est&aacute;tica do **Publish**

### M&eacute;todos

+ **run**: Abre a conex&atilde;o com o RabbitMQ, sendo **name**, **url** obrigat&oacute;rios
 - **name**: Nome do **Engine**
 - **url**: Url para conectar ao RabbitMQ

+ **channel**: Traz um **Channel** ou cria um novo caso n&atilde;o exista, sendo **name**, **future** obrigat&oacute;rios
 - **name**: Nome do **Channel**
 - **future**: Futuro que ser&aacute; completado com o *channel* aberto
 - **prefetch_count**: Mensagem consumidas simultaneamente (valor padr&atilde;o **10**)

+ **connection**: Traz o **Engine** quando a conex&atilde;o estiver aberta
 - **future**: Futuro que ser&aacute; completado com o **Engine**

## Consumer

Consumidor do RabbitMQ

```
- tornpack
 - rabbitmq
  - engine
   - consumer
    Consumer
```

### M&eacute;todos

+ **cancel**: Cancela o consumidor, sendo **channel**, **consumer_tag** obrigat&oacute;rios
 - **channel**: **channel**
 - **consumer_tag**: *tag* do consumidor
 - **nowait**: Aguarda a opera&ccedil;&atilde;o no RabbitMQ ser conclu&iacute;da para chamar o futuro (valor padr&atilde;o **False**)
 - **future**: Futuro que ser&aacute; completado quando o consumidor for fechado

+ **consume**: Declara um consumidor para uma fila do RabbitMQ, sendo **channel**, **consumer_callback**, **queue** obrigat&oacute;rios
 - **channel**: **channel**
 - **consumer_callback**: *Callback* que ser&aacute; chamado quando uma mensagem chegar a fila do RabbitMQ
 - **queue**: Nome da fila
 - **no_ack**: Se o consumidor vai aguardar o **ack** para remover a mensagem da fila (valor padr&atilde;o **False**)
 - **exclusive**: Se a fila ser&aacute; exclusiva (valor padr&atilde;o **False**)
 - **consumer_tag**: *tag* do consumidor (gera um valor aleat&oacute;rio caso n&atilde;o seja passado)
 - **arguments**: Argumentos do consumidor

## Exchange

*Exchange* do RabbitMQ

```
- tornpack
 - rabbitmq
  - engine
   - exchange
    Exchange
```

### M&eacute;todos

+ **bind**: Vincula uma *exchange* a outra, sendo **channel**, **destination**, **source**, **routing_key** obrigat&oacute;rios
 - **channel**: **channel**
 - **destination**: Nome da *exchange* de destino
 - **source**: Nome da *exchange* de origem
 - **routing_key**: Rota que far&aacute; a mensagem ir para a *exchange* de destino
 - **arguments**: Headers que far&atilde;o a mensagem ir para a *exchange* de destino
 - **future**: Futuro que ser&aacute; completado quando o v&iacute;nculo for feito

+ **declare**: Declara uma nova *exchange*, sendo **channel**, **exchange** obrigat&oacute;rios
 - **channel**: **channel**
 - **exchange**: Nome da *exchange*
 - **exchange_type**: Tipo da *exchange* (valor padr&atilde;o **direct**)
 - **passive**: Se somente far&aacute; a checagem que a *exchange* existe (valor padr&atilde;o **False**)
 - **durable**: Se a *exchange* &eacute; duravel (valor padr&atilde;o **False**)
 - **auto_delete**: Se a *exchange* ser&aacute; deletada automaticamente (valor padr&atilde;o **False**)
 - **internal**: Se a *exchange* &eacute; interna (valor padr&atilde;o **False**)
 - **arguments**: Argumentos da *exchange*
 - **future**: Futuro que ser&aacute; completado quando a *exchange* for declarada

+ **delete**: Remove uma *exchange* do RabbitMQ, sendo **channel**, **exchange** obrigat&oacute;rios
 - **channel**: **channel**
 - **if_unused**: Somente remove a *exchange* se n&atilde;o foi usada (valor padr&atilde;o **False**)
 - **future**: Futuro que ser&aacute; completado quando a *exchange* for removida

+ **unbind**: Desfaz um v&iacute;nculo de uma *exchange* com outra, sendo **channel**, **destination**, **source**, **routing_key** obrigat&oacute;rios
 - **channel**: **channel**
 - **destination**: Nome da *exchange* de destino
 - **source**: Nome da *exchange* de origem
 - **routing_key**: Rota do v&iacute;nculo da *exchange* de destino
 - **arguments**: Headers do v&iacute;nculo da *exchange* de destino
 - **future**: Futuro que ser&aacute; completado quando o v&iacute;nculo for desfeito

## Queue

Fila do RabbitMQ

### M&eacute;todos

+ **bind**: Vincula uma fila a uma *exchange*, sendo **channel**, **queue**, **exchange**, **routing_key** obrigat&oacute;rios
 - **channel**: **channel**
 - **queue**: Nome da fila
 - **exchange**: Nome da *exchange*
 - **routing_key**: Rota do v&iacute;nculo para a fila
 - **arguments**: *Headers* do v&iacute;nculo para a fila
 - **future**: Futuro que ser&aacute; completado com o resultado da opera&ccedil;&atilde;o

+ **declare**: Cria uma fila, sendo **channel**, **queue**
 - **channel**: **channel**
 - **queue**: Nome da fila
 - **durable**: Se a fila &eacute; duravel (valor padr&atilde;o **False**)
 - **exclusive**: Se a fila ser&aacute; exclusiva (valor padr&atilde;o **True**)
 - **auto_delete**: Se a fila ser&aacute; deletada automaticamente (valor padr&atilde;o **True**)
 - **future**: Futuro que ser&aacute; completado com o resultado da opera&ccedil;&atilde;o
 - **arguments**: Argumentos da fila

+ **delete**: Remove uma fila, sendo **channel**, **queue** obrigat&oacute;rios
 - **channel**: **channel**
 - **queue**: Nome da fila
 - **if_unused**: Somente remove a fila se n&atilde;o foi usada (valor padr&atilde;o **False**)
 - **if_empty**: Somente remove a fila se n&atilde;o houver mensagens aguardando (valor padr&atilde;o **False**
 - **future**: Futuro que ser&aacute; completado com o resultado da opera&ccedil;&atilde;o

+ **purge**: Limpa a fila, sendo **channel**, **queue** obrigat&oacute;rios
 - **channel**: **channel**
 - **queue**: Nome da fila
 - **future**: Futuro que ser&aacute; completado com o resultado da opera&ccedil;&atilde;o

+ **unbind**: Desvincula uma fila de uma *exchange*, sendo **channel**, **queue**, **exchange**, **routing_key** obrigat&oacute;rios
 - **channel**: **channel**
 - **queue**: Nome da fila
 - **exchange**: Nome da *exchange*
 - **routing_key**: Rota do v&iacute;nculo para a fila
 - **arguments**: *Headers* do v&iacute;nculo para a fila
 - **future**: Futuro que ser&aacute; completado com o resultado da opera&ccedil;&atilde;o

## Publish

*Publish* do RabbitMQ

### M&eacute;todos

+ **push**: Envia uma mensagem a RabbitMQ, sendo **channel**, **body**, **exchange**, **routing_key** obrigat&oacute;rios
 - **channel**: **channel**,
 - **body**: Corpo da mensagem
 - **exchange**: Nome da *exchange*
 - **routing_key**: Rota da mensagem
 - **properties**: Propriedades da mensagem
 - **future**: Futuro que ser&aacute; completado quando a mensagem for enviada

# RAID

## HTTP

**Websocket** *handler*
+ O **Websocket** deve herdar a classe **Handler** do RAID

```
- tornpack
 - raid
  - http
   Handler
```

Exemplo:

```python
from tornpack.raid.http import Handler

class WSHandler(Handler):
    ...
```

### Atributos

+ **actions**: **Dict** sendo a chave a **action** e o valor um **actor**
 - get: Retorna todos as **actions**
 - set: Sobreescreve as **actions**

+ **kernel**: Retorna o **Kernel**

+ **name**: Nome da conex&atilde;o
 - get: Retorna o nome da conex&atilde;o (valor padr&atilde;o **raid**)
 - set: Define o nome da conex&atilde;o

+ **secret**: Segredo **JWT** da conex&atilde;o
 - get: Retorna o segredo (valor padr&atilde;o &eacute; o segredo principal do raid)
 - set: Define o segredo

### Hooks

+ **\_\_on_close__**: Chamado assim que a conex&atilde;o for fechada

+ **\_\_on_disconnect__**: Chamado assim que desconectar

+ **\_\_on_open__**: Chamado assim que a conex&atilde;o for aberta

### M&eacute;todos

+ **disconnect**: Desconecta o **Websocket**

+ **push**: Envia a **message** ao *client*, sendo **message** obrigat&oacute;rio
 - **message**: Mensagem a ser enviada

+ **to_push**: Retorna um futuro que quando completado envia o resultado ao *client*

+ **utag**: **Hash** &uacute;nico gerada a partir do **etag**
 - **etag**: **Hash** &uacute;nico recebida do *client*

## Kernel

RAID **kernel**

```
- tornpack
 - raid
  Kernel
```

### Atributos

+ **connections**: **Dict** sendo a chave o **uuid4** e valor a conex&atilde;o **Websocket HTTP**

+ **groups**: **Dict** sendo a chave o nome e valor a lista de conex&otilde;es **Websocket HTTP** que fazem parte do grupo

### M&eacute;todos

+ **group_add**: Adiciona uma conex&atilde;o ao grupo, sendo todos os par&acirc;metros obrigat&oacute;rios
 - **group**: Nome do grupo
 - **io**: Conex&atilde;o **Websocket**

+ **group**: Para cada conex&atilde;o do grupo chama o *callback*, sendo todos os par&acirc;metros obrigat&oacute;rios
 - **group**: Nome do grupo
 - **foreach**: *Callback* que ser&aacute; chamado, deve receber a conex&atilde;o como par&acirc;metro
 - **future**: Futuro que ser&aacute; completado quando todos as conex&otilde;es forem iteradas

+ **group_broadcast**: Para cada conex&atilde;o do grupo envia **msg**, sendo todos os par&acirc;metros obrigat&oacute;rios
 - **group**: Nome do grupo
 - **msg**: Mensagem a ser enviada
 - **future**: Futuro que ser&aacute; completado quando todos as conex&otilde;es forem iteradas

+ **group_del**: Remove uma conex&atilde;o do grupo, sendo todos os par&acirc;metros obrigat&oacute;rios
 - **group**: Nome do grupo
 - **io**: Conex&atilde;o **Websocket**

# Raven

Ferramenta para *logging* no console
+ Todos os m&eacute;todos recebem uma **string** que ser&aacute; exibida junto do *log*

```
- tornpack
 - raven
  Raven
```

## M&eacute;todos

+ **critical**: *Log* do tipo cr&iacute;tico

+ **debug**: *Log* do tipo **debug**

+ **error**: *Log* do tipo erro

+ **info**: *Log* do tipo informa&ccedil;&atilde;o

+ **warning**: *Log* do tipo alerta

# Swarm

**WIP**

# Refer&ecirc;ncias

+ [Motor](http://motor.readthedocs.org/en/stable/index.html)
+ [Python](https://www.python.org)
+ [RabbitMQ](https://www.rabbitmq.com)
+ [Tornado](http://www.tornadoweb.org/en/stable/index.html)
+ [Tornpack](https://gitlab.com/vikingmakt/tornpack)
